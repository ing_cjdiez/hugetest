package com.huge.test.common.states

import com.huge.test.common.errors.NetworkErrorType
import com.huge.test.common.interfaces.State

sealed class DataViewState : State {

    /**
     * Represents loading state
     */
    object Loading : DataViewState()
    object FinishLoading : DataViewState()

    /**
     * Represents no data
     */
    object NoData : DataViewState()

    /**
     * Represents a successful load
     */
    class HasData<T>(var dataList: List<T>) : DataViewState()
    class HasSingleData<T>(var data: T) : DataViewState()
    class HasListData<T>(var data: T) : DataViewState()

    /**
     * Represents a failed load
     */
    class Error(var networkErrorType: NetworkErrorType) : DataViewState()
}