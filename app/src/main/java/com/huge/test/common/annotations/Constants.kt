package com.huge.test.common.annotations

object Constants {
    const val UI = "ui"
    const val IO = "io"

    //currencyconverterapi.com
    const val apiKey = "d0d0a4bf2fe34c745b55"
    const val underscore = "_"

    const val USD = "USD"
}