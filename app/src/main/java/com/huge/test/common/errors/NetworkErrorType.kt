package com.huge.test.common.errors

enum class NetworkErrorType {
    TIMEOUT,
    UNKNOWN
}