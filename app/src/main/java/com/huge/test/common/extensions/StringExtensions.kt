package com.huge.test.common.extensions

import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

fun String.toMoneyFormat(): String{
    val formatter: NumberFormat = DecimalFormat("###,###,##0.00");
    return "$${formatter.format(this.toDouble())}"
}

fun getTodayDate(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    return sdf.format(Date()).toString()
}

fun getLastWeekDate(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    return sdf.format(getDaysAgo(7)).toString()
}

private fun getDaysAgo(daysAgo: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)
    return calendar.time
}
