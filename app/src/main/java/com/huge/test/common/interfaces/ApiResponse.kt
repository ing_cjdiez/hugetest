package com.huge.test.common.interfaces

interface ApiResponse<T, E>{
    fun onResponseReceived(response: T)
    fun onFailure(error: E)
}