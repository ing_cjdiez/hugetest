package com.huge.test.common.interfaces

interface ObservableCallback {
    fun onStart()
    fun onFinish()
}