package com.huge.test.models

import com.google.gson.annotations.JsonAdapter
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Conversion(
    val from: String,
    val to: String
    /*
    @Json(name = from.plus("_").plus(to)) val data: Double
    */
): Serializable