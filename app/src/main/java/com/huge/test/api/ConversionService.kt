package com.huge.test.api

import com.huge.test.BuildConfig
import io.reactivex.Observable
import retrofit2.http.GET
import com.huge.test.common.annotations.Constants.apiKey
import retrofit2.http.Query

interface ConversionService {
    @GET(BuildConfig.CONVERT_ENDPOINT)
    fun convertCurrency(@Query("q") query: String, @Query("apiKey") apikey: String = apiKey): Observable<Map<String, Double>>

    @GET(BuildConfig.CONVERT_ENDPOINT)
    fun currencyHistory(
        @Query("q") query: String, @Query("apiKey") apikey: String = apiKey,
        @Query("date") startDate: String, @Query("endDate") endDate: String
    ): Observable<Map<String, Map<String, Double>>>
}