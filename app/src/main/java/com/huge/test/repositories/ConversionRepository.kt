package com.huge.test.repositories

import com.huge.test.api.ConversionService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConversionRepository @Inject constructor(private val conversionService: ConversionService) {
    fun convertCurrency(query: String) = conversionService.convertCurrency(query)
    fun currencyHistory(query: String, startDate: String, endDate: String) =
        conversionService.currencyHistory(query, startDate = startDate, endDate = endDate).map {
            it.get(query)
        }
}