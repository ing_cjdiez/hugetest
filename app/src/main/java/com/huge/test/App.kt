package com.huge.test

import android.app.Activity
import androidx.multidex.MultiDexApplication
import com.huge.test.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : MultiDexApplication(), HasActivityInjector{

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun onCreate() {
        super.onCreate()
        initDaggger()
    }

    private fun initDaggger(){
        DaggerAppComponent.builder().create(this).inject(this)
    }
}