package com.huge.test.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.huge.test.R
import com.huge.test.common.annotations.Constants.USD
import com.huge.test.common.extensions.hideKeyboard
import com.huge.test.common.extensions.activityViewModelProvider
import com.huge.test.common.extensions.toMoneyFormat
import com.huge.test.common.states.DataViewState
import com.huge.test.databinding.ActivityMainBinding
import com.huge.test.ui.main.MainViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var viewModel: MainViewModel

    private var binding: ActivityMainBinding? = null

    private var currencyList = arrayOf("GBP", "EUR", "JPY", "BRL")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setupViewModel()
        setupUi()
    }

    private fun setupViewModel() {
        viewModel = activityViewModelProvider(modelFactory)
        viewModel.viewState.observe(this, Observer { handleStateChange(it) })
    }

    private fun setupUi() {
        binding?.let {
            val spAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, currencyList)
            spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            it.spCurrenciesTo.adapter = spAdapter

            it.btConvert.setOnClickListener { view ->
                view.hideKeyboard()
                if( it.etUsd.text.isNullOrEmpty() ) {
                    Toast.makeText(this, R.string.type_amount_error, Toast.LENGTH_SHORT).show()
                } else {
                    viewModel.convertCurrency(
                        USD,
                        it.etUsd.text.toString().toInt(),
                        it.spCurrenciesTo.selectedItem.toString()
                    )
                }
            }
        }
    }

    private fun handleStateChange(state: DataViewState){
        when(state){
            is DataViewState.Loading -> { binding?.tvLoading?.visibility = View.VISIBLE }
            is DataViewState.FinishLoading -> { binding?.tvLoading?.visibility = View.GONE }
            is DataViewState.HasSingleData<*> -> {
                binding?.let {
                    it.tvResult?.text = (state as DataViewState.HasSingleData<String>).data.toMoneyFormat()
                }
            }
            is DataViewState.HasListData<*> -> {
                showChart((state as DataViewState.HasListData<Map<String, Double>>).data)
            }
            is DataViewState.Error -> {
                Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_LONG).show()
            }
            is DataViewState.NoData -> {
                Toast.makeText(this, R.string.info_not_found, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showChart(data: Map<String, Double>) {
        binding?.let {
            val entries = ArrayList<BarEntry>()
            var cont = 0
            data.forEach {
                item -> entries.add(BarEntry(cont++.toFloat(), (item.value*it.etUsd.text.toString().toInt()).toFloat()))
            }

            val dataSet = BarDataSet(entries, getString(R.string.last_seven_days))
            //dataSet.setColor(...)
            //dataSet.setValueTextColor(...)
            it.chart.data = BarData(dataSet)
            it.chart.invalidate()
        }
    }
}
