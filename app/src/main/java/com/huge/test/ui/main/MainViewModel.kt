package com.huge.test.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.huge.test.common.annotations.Constants
import com.huge.test.common.annotations.Constants.IO
import com.huge.test.common.annotations.Constants.UI
import com.huge.test.common.errors.NetworkErrorType
import com.huge.test.common.extensions.getLastWeekDate
import com.huge.test.common.extensions.getTodayDate
import com.huge.test.common.interfaces.ApiResponse
import com.huge.test.common.interfaces.ObservableCallback
import com.huge.test.common.states.DataViewState
import com.huge.test.repositories.ConversionRepository
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import javax.inject.Named

class MainViewModel @Inject constructor(
    private val conversionRepository: ConversionRepository,
    @Named(UI) private val uiScheduler: Scheduler,
    @Named(IO) private val ioScheduler: Scheduler
): ViewModel(), ApiResponse<Map<String, Double>, Throwable>, ObservableCallback {

    var viewState = MutableLiveData<DataViewState>()
    private var queryConversion = ""
    private var amountConversion = 0

    fun convertCurrency(currencyFrom: String, amount: Int, currencyTo: String) {
        amountConversion = amount
        queryConversion = "$currencyFrom${Constants.underscore}$currencyTo"

        conversionRepository.convertCurrency(queryConversion)
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                {
                    onResponseReceived(it)
                    getCurrencyHistory(currencyFrom, currencyTo)
                },
                { onFailure(it) },
                { viewState.value = DataViewState.FinishLoading },
                { viewState.value = DataViewState.Loading }
            )
    }

    fun getCurrencyHistory(currencyFrom: String, currencyTo: String, startDate: String = getLastWeekDate(), endDate: String = getTodayDate()) {
        queryConversion = "$currencyFrom${Constants.underscore}$currencyTo"
        conversionRepository.currencyHistory(queryConversion, startDate, endDate)
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                {
                    onResponseReceived(it!!) },
                { onFailure(it) },
                { viewState.value = DataViewState.FinishLoading },
                { viewState.value = DataViewState.Loading }
            )
    }

    override fun onResponseReceived(response: Map<String, Double>) {
        if(response.isNotEmpty()) {
            if(response.size>1) {
                viewState.value = DataViewState.HasListData(response)
            } else {
                viewState.value =
                    DataViewState.HasSingleData((response[queryConversion]?.times(amountConversion)).toString())
            }
        } else {
            viewState.value = DataViewState.NoData
        }
    }

    override fun onFailure(error: Throwable) {
        val errorType = if (error is TimeoutException) NetworkErrorType.TIMEOUT else NetworkErrorType.UNKNOWN
        viewState.value = DataViewState.Error(errorType)
    }

    override fun onStart() {
        viewState.value = DataViewState.Loading
    }

    override fun onFinish() {
        viewState.value = DataViewState.FinishLoading
    }
}