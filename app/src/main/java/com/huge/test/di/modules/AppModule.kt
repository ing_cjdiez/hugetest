package com.huge.test.di.modules

import android.content.Context
import com.huge.test.App
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: App): Context {
        return application.applicationContext
    }
}