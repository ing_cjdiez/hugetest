package com.huge.test.di.components

import com.huge.test.App
import com.huge.test.di.modules.ActivityModule
import com.huge.test.di.modules.ApiModule
import com.huge.test.di.modules.AppModule
import com.huge.test.di.modules.NetworkModule
import com.huge.test.di.modules.SchedulerModule
import com.huge.test.di.modules.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        ApiModule::class,
        AppModule::class,
        NetworkModule::class,
        SchedulerModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent: AndroidInjector<App>{
    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<App>()
}