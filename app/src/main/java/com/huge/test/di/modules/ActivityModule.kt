package com.huge.test.di.modules

import com.huge.test.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule{

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity
}