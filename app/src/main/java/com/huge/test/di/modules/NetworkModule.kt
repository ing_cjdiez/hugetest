package com.huge.test.di.modules

import com.huge.test.BuildConfig
import com.huge.test.common.annotations.Constants.IO
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
object NetworkModule {

    /**
     * Provides the Moshi object.
     * @return the Moshi object
     */
    @Provides
    @Singleton
    @JvmStatic
    internal fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Provides
    @Singleton
    @JvmStatic
    internal fun provideOkHttpClient(
        //@Named(HEADER) headerInterceptor: Interceptor
    ): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        //clientBuilder.addInterceptor(headerInterceptor)
        return clientBuilder.build()
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Singleton
    @JvmStatic
    internal fun provideRetrofitInterface(
        okHttpClient: OkHttpClient,
        moshi: Moshi,
        @Named(IO) scheduler: Scheduler
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(scheduler))
        .client(okHttpClient)
        .build()

}