package com.huge.test.di.modules

import com.huge.test.api.ConversionService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
object ApiModule {

    @Provides
    @Singleton
    @JvmStatic
    internal fun provideConvertService(retrofit: Retrofit): ConversionService{
        return retrofit.create(ConversionService::class.java)
    }
}